﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using PersonalWebsite.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using PersonalWebsite.Models.AuthModels;

namespace PersonalWebsite.Controllers
{
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        // Okta Settings so they can be used throughout
        private readonly IOptions<OktaAuth> oktaApp;
        private readonly IOptions<OktaAuth> oktaUser;

        public AuthenticationController(IOptions<OktaAuth> oktaApp, IOptions<OktaAuth> oktaUser)
        {
            this.oktaApp = oktaApp;
            this.oktaUser = oktaUser;
        }

        [Route("api/oauth2/token")]
        [HttpPost("api/oauth2/token")]
        public async Task<ActionResult<object>> PostAsync([FromForm]OktaSettings formData)
        {
            // required variables
            string clientId = "";
            string clientSecret = "";
            string tokenUrl = "";

            // Initialize the client instance.
            var client = new HttpClient();

            // Initialize token instance
            var token = new OktaToken();

            // Construct the data we will send to the auth server.
            var postMessage = new Dictionary<string, string>();
            postMessage.Add("scope", formData.scope);
            Debug.WriteLine(formData.username == null || formData.password == null);
            if (formData.username == null || formData.password == null)
            {
                clientId = formData.clientId;
                clientSecret = formData.clientSecret;
                tokenUrl = this.oktaApp.Value.TokenUrl;
                postMessage.Add("grant_type", "client_credentials");
            }
            else
            {
                clientId = this.oktaUser.Value.ClientId;
                clientSecret = this.oktaUser.Value.ClientSecret;
                tokenUrl = this.oktaUser.Value.TokenUrl;
                postMessage.Add("grant_type", "password");
                postMessage.Add("username", formData.username);
                postMessage.Add("password", formData.password);
            }

            Debug.WriteLine(clientId);
            Debug.WriteLine(clientSecret);
            Debug.WriteLine(tokenUrl);

            // Create the okta auth token to make requests to the api.
            var clientCreds = System.Text.Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", System.Convert.ToBase64String(clientCreds));

            // Create the request
            var request = new HttpRequestMessage(HttpMethod.Post, tokenUrl)
            {
                Content = new FormUrlEncodedContent(postMessage)
            };

            // Make a call to get the access token
            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                var json = await response.Content.ReadAsStringAsync();
                token = JsonConvert.DeserializeObject<OktaToken>(json);
                token.ExpiresAt = DateTime.UtcNow.AddSeconds(token.ExpiresIn);
            }
            else
            {
                return ("Unable to retrieve access token from Okta");
            }

            // Return data to the user so they can use it on subsequent requests.
            return (token);
        }
    }
}

