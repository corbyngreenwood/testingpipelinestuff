﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PersonalWebsite.Models;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Newtonsoft.Json;
using PersonalWebsite.Models.Blogs;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace PersonalWebsite.Controllers
{
    [ApiController]
    public class OrganisationController : ControllerBase
    {
        private readonly CorbynDbContext _context;

        public OrganisationController(CorbynDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get a list of organisations this user has access too read/write to.
        /// </summary>
        /// <returns>List of organisations the user can work with</returns>
        [HttpGet("api/blogs")]
        public IActionResult GetOrganisation()
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, JsonConvert.SerializeObject(new Models.Responses.ResponseStyle(501, "Not Implemented")));
        }

        /// <summary>
        /// Get more details of a specific organisation, including the organisations children.
        /// </summary>
        /// <param name="id">ID of the organisation</param>
        /// <returns>details about the organisation and a list of its children</returns>
        [HttpGet("api/blogs/{id}")]
        public async Task<IActionResult> GetOrganisation([FromRoute] int id)
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, JsonConvert.SerializeObject(new Models.Responses.ResponseStyle(501, "Not Implemented")));
        }

        /// <summary>
        /// Edits the details of the specified organisation
        /// </summary>
        /// <param name="id">ID of the organisation getting edited</param>
        /// <param name="organisationDetails">The data for the edit request</param>
        /// <returns>An object containing all the details of the updated organisation</returns>
        [HttpPatch("api/blogs/{id}")]
        [Authorize(Policy = "blog.readwrite")]
        public async Task<IActionResult> PatchOrganisation([FromRoute] int id, [FromBody] Blog organisationDetails)
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, JsonConvert.SerializeObject(new Models.Responses.ResponseStyle(501, "Not Implemented")));
        }

        /// <summary>
        /// Creates a new organisation
        /// </summary>
        /// <param name="organisationDetails">The data required to create the organisation</param>
        /// <returns>An object containing all the details of the updated organisation</returns>
        [HttpPost("api/blogs")]
        [Authorize(Policy = "global.admin")]
        public async Task<IActionResult> PostOrganisation([FromBody] Blog organisationDetails)
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, JsonConvert.SerializeObject(new Models.Responses.ResponseStyle(501, "Not Implemented")));
        }

        /// <summary>
        /// Deleted an organisation
        /// ADMIN ONLY
        /// </summary>
        /// <param name="id">ID of the organisation to get deleted</param>
        /// <returns>No Content</returns>
        [HttpDelete("api/blogs/{id}")]
        [Authorize(Policy = "global.admin")]
        public async Task<IActionResult> DeleteOrganisation([FromRoute] int id)
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, JsonConvert.SerializeObject(new Models.Responses.ResponseStyle(501, "Not Implemented")));
        }

        /// <summary>
        /// Checks if an organsation with a specific id already exists
        /// </summary>
        /// <param name="id">ID of the organisation getting checked</param>
        /// <returns>True if exists or False if not</returns>
        [NonAction]
        private bool OrganisationDetailsExists(int id)
        {
            return _context.Blog.Any(e => e.BlogId == id); 
        }
    }
}
