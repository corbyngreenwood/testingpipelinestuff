﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PersonalWebsite.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using PersonalWebsite.Models.AuthModels;

namespace PersonalWebsite
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Allow CORS
            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                });
            });

            // Configure Authentication
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                options.Authority = "https://dev-472799.okta.com/oauth2/default";
                options.Audience = "api://default";
                options.RequireHttpsMetadata = false;
            });

            // Configure policies to verify scopes on api endpoints
            services.AddAuthorization(options =>
            {
                // Global Admin Policy
                options.AddPolicy("global.admin", policy => policy.RequireAssertion(context => context.User.HasClaim(c => c.Type == "http://schemas.microsoft.com/identity/claims/scope" && (c.Value == "global.admin"))));

                // bloganisation Policies
                options.AddPolicy("blog.read", policy => policy.RequireAssertion(context => context.User.HasClaim(c => c.Type == "http://schemas.microsoft.com/identity/claims/scope" && (c.Value == "blog.read" || c.Value == "blog.readwrite" || c.Value == "global.admin"))));
                options.AddPolicy("blog.readwrite", policy => policy.RequireAssertion(context => context.User.HasClaim(c => c.Type == "http://schemas.microsoft.com/identity/claims/scope" && (c.Value == "blog.readwrite" || c.Value == "global.admin"))));
                options.AddPolicy("blog.admin", policy => policy.RequireAssertion(context => context.User.HasClaim(c => c.Type == "http://schemas.microsoft.com/identity/claims/scope" && (c.Value == "global.admin"))));
            });

            // Configure app to use database
            var connection = Configuration.GetSection("ConnectionStrings:DefaultConnection").Value;
            services.AddDbContext<CorbynDbContext>(options => options.UseSqlServer(connection));

            services.Configure<OktaAuth>(Configuration.GetSection("OktaApp"));
            services.Configure<OktaAuth>(Configuration.GetSection("OktaUser"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors(MyAllowSpecificOrigins);
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}
