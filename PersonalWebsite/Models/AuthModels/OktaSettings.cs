﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalWebsite.Models.AuthModels
{
    public class OktaSettings
    {
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string scope { get; set; }
    }
}
