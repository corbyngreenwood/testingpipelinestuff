﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalWebsite.Models
{
    public class CorbynDbContext : DbContext
    {
        public CorbynDbContext(DbContextOptions<CorbynDbContext> options) : base(options) { }

        public DbSet<PersonalWebsite.Models.Blogs.Blog> Blog { get; set; }
    }
}
