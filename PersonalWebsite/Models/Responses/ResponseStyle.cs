﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalWebsite.Models.Responses
{
    public class ResponseStyle
    {
        public ResponseStyle(int code, string message)
        {
            this.StatusCode = code;
            this.Message = message;
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
